package com.bluea.server.controller;

import java.util.List;

import com.bluea.server.pojo.dto.ResponseDto;
import com.bluea.server.pojo.entity.Answer;
import com.bluea.server.service.AnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class AnswerController {

    private final AnswerService answerService;

    @PostMapping("/answer/add")
    public ResponseDto<Void> addAnswers(@RequestBody List<Answer> answers) {
        answerService.insertAnswers(answers);
        return ResponseDto.success();
    }

}
