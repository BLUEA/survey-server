package com.bluea.server.controller;

import javax.validation.Valid;

import com.bluea.server.pojo.dto.QuestionnaireDto;
import com.bluea.server.pojo.dto.ResponseDto;
import com.bluea.server.pojo.vo.CreateQuestionnaireVo;
import com.bluea.server.pojo.vo.DeleteQuestionnaireVo;
import com.bluea.server.pojo.vo.FindQuestionnaireVo;
import com.bluea.server.pojo.vo.QuestionVo;
import com.bluea.server.pojo.vo.UpdateQuestionVo;
import com.bluea.server.pojo.vo.UpdateQuestionnaireVo;
import com.bluea.server.service.QuestionnaireService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class QuestionnaireController {

    private final QuestionnaireService questionnaireService;

    @PostMapping("/questionnaire/create")
    public ResponseDto<QuestionnaireDto> createQuestionnaire(
            @RequestBody @Valid CreateQuestionnaireVo vo) {
        return ResponseDto.success(questionnaireService.createQuestionnaire(vo));
    }

    @PostMapping("/questionnaire/get")
    public ResponseDto<QuestionnaireDto> getQuestionnaire(
            @RequestBody @Valid FindQuestionnaireVo vo) {
        return ResponseDto.success(questionnaireService.getQuestionnaire(vo.getQuestionnaireId()));
    }

    @PostMapping("/questionnaire/update")
    public ResponseDto<Void> updateQuestionnaire(@RequestBody @Valid UpdateQuestionnaireVo vo) {
        questionnaireService.updateQuestionnaire(vo);
        return ResponseDto.success();
    }

    @PostMapping("/questionnaire/delete")
    public ResponseDto<Void> deleteQuestionnaire(@RequestBody @Valid DeleteQuestionnaireVo vo){
        questionnaireService.deleteQuestionnaire(vo.getId());
        return ResponseDto.success();
    }

    @PostMapping("/question/add")
    public ResponseDto<Void> addQuestion(@RequestBody @Valid QuestionVo vo) {
        questionnaireService.addQuestion(vo);
        return ResponseDto.success();
    }

    @PostMapping("/question/update")
    public ResponseDto<Void> updateQuestion(@RequestBody @Valid UpdateQuestionVo vo) {
        questionnaireService.updateQuestion(vo);
        return ResponseDto.success();
    }

    @PostMapping("/question/delete")
    public ResponseDto<Void> updateQuestion(@RequestBody @Valid DeleteQuestionnaireVo vo) {
        questionnaireService.deleteQuestion(vo.getId());
        return ResponseDto.success();
    }
}
