package com.bluea.server.controller;

import java.text.MessageFormat;

import com.bluea.server.exception.ApplicationException;
import com.bluea.server.exception.BusinessExceptionCode;
import com.bluea.server.pojo.dto.AccessTokenDto;
import com.bluea.server.pojo.dto.ResponseDto;
import com.bluea.server.pojo.vo.LoginUserInfoVo;
import com.bluea.server.pojo.vo.UserInfoVo;
import com.bluea.server.service.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class LoginController {

    private final LoginService loginService;

    @PostMapping("/auth/login")
    public ResponseDto<AccessTokenDto> login(@RequestBody LoginUserInfoVo vo) {
        return ResponseDto.success(loginService.login(vo));
    }

    @PostMapping("/auth/register")
    public ResponseDto<Void> register(@RequestBody UserInfoVo vo) {
        return loginService.register(vo) ? ResponseDto.success() :
                ResponseDto.fail(BusinessExceptionCode.REGISTER_FAIL.getCode(),
                        MessageFormat.format(BusinessExceptionCode.REGISTER_FAIL.getMessage(),
                                "something wrong"));
    }

}
