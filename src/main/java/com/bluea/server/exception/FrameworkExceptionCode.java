package com.bluea.server.exception;

public enum FrameworkExceptionCode implements ErrorWrapper {

    SOME_FRAMEWORK_ERROR(20000, "something framework error"),
    PARAMETER_ILLEGAL(20001, "Parameter illegal: {0}"),
    ENUM_PARSE_ERROR(20002, "枚举转换错误: {0}")
    ;


    Integer code;

    String message;

    FrameworkExceptionCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return null;
    }

    @Override
    public Integer getCode() {
        return null;
    }

    public static FrameworkException reException(Integer code, String message) {
        return new FrameworkException(code, message);
    }
}
