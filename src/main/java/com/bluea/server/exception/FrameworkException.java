package com.bluea.server.exception;

public class FrameworkException extends ApplicationException {

    FrameworkException() {
        super();
    }

    FrameworkException(String message, Throwable cause) {
        super(message, cause);
    }

    FrameworkException(Integer code, String message, Throwable cause) {
        super(code, message, cause);
    }

    FrameworkException(Integer code, String message) {
        super(code, message);
    }
}
