package com.bluea.server.exception.resolver;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bluea.server.exception.BusinessExceptionCode;
import com.bluea.server.pojo.dto.ResponseDto;
import com.bluea.server.util.JsonUtil;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException)
            throws IOException, ServletException {
        response.setStatus(200);
        response.setContentType("application/json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().print(JsonUtil.toJson(
                ResponseDto.fail(BusinessExceptionCode.UNAUTHENTICATED.getCode(),
                        BusinessExceptionCode.UNAUTHENTICATED.getMessage())));
    }
}
