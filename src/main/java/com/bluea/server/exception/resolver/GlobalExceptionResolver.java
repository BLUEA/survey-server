package com.bluea.server.exception.resolver;

import java.text.MessageFormat;

import com.bluea.server.exception.ApplicationException;
import com.bluea.server.exception.BusinessExceptionCode;
import com.bluea.server.pojo.dto.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@ControllerAdvice
public class GlobalExceptionResolver {

    @ResponseBody
    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ResponseDto<Void>> resolveApplicationException(ApplicationException ex) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(ResponseDto.fail(ex.getCode(), ex.getMessage()));
    }

    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseDto<Void>> resolveMethodArgumentNotValidException(
            MethodArgumentNotValidException ex) {

        StringBuffer errMsgStringBuffer = new StringBuffer();
        ex.getBindingResult().getFieldErrors().forEach(x -> {
            errMsgStringBuffer.append(x.getField()).append(x.getDefaultMessage()).append(";");
        });

        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ResponseDto.fail(BusinessExceptionCode.BAD_REQUEST_BODY.getCode(),
                        MessageFormat.format(BusinessExceptionCode.BAD_REQUEST_BODY.getMessage(),
                                errMsgStringBuffer.toString())));
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseDto<Void>> resolveException(Exception ex) {
        log.error(ex.getMessage());
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ResponseDto.fail(0, ex.getMessage()));
    }

}
