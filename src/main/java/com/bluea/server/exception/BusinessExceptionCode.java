package com.bluea.server.exception;

public enum BusinessExceptionCode implements ErrorWrapper {

    SOME_BUSINESS_ERROR(10000, "something business error"),
    LOGIN_FAIL(10001, "登陆失败: {0}"),
    REGISTER_FAIL(10002, "注册失败: {0}"),
    INCORRECT_ACCOUNT(10003, "用户名错误"),
    ACCOUNT_LOCK(10004, "账户已被锁定"),
    UNAUTHENTICATED(10005, "未认证"),
    BAD_REQUEST_BODY(10006, "请求对象不符合: {0}"),
    CREATE_QUESTIONNAIRE_FAIL(10007, "创建问卷失败"),
    QUESTIONNAIRE_NOT_FOUND(10008, "未找到问卷"),
    SAVE_QUESTION_FAIL(10009, "保存问题失败"),
    QUESTIONNAIRE_UPDATE_FAIL(10010, "问卷修改失败")

    ;

    private Integer code;
    private String message;

    BusinessExceptionCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    public static BusinessException reException(Integer code, String message) {
        return new BusinessException(code, message);
    }

    public static BusinessException reException(BusinessExceptionCode exceptionEnum) {
        return new BusinessException(exceptionEnum.code, exceptionEnum.message);
    }

    public static BusinessException reException(BusinessExceptionCode exceptionEnum,
                                                Throwable cause) {
        return new BusinessException(exceptionEnum.code, exceptionEnum.message, cause);
    }
}
