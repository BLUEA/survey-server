package com.bluea.server.service.impl;

import java.util.Objects;
import java.util.Optional;

import com.bluea.server.common.Constant;
import com.bluea.server.common.CurrentUserContext;
import com.bluea.server.dao.QuestionDao;
import com.bluea.server.dao.QuestionnaireDao;
import com.bluea.server.dao.UserInfoDao;
import com.bluea.server.exception.BusinessExceptionCode;
import com.bluea.server.pojo.dto.QuestionnaireDto;
import com.bluea.server.pojo.entity.Question;
import com.bluea.server.pojo.entity.Questionnaire;
import com.bluea.server.pojo.mapper.ModelMapper;
import com.bluea.server.pojo.vo.CreateQuestionnaireVo;
import com.bluea.server.pojo.vo.QuestionVo;
import com.bluea.server.pojo.vo.UpdateQuestionVo;
import com.bluea.server.pojo.vo.UpdateQuestionnaireVo;
import com.bluea.server.service.QuestionnaireService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class QuestionnaireServiceImpl implements QuestionnaireService {

    private final QuestionnaireDao questionnaireDao;
    private final UserInfoDao userInfoDao;
    private final QuestionDao questionDao;
    private final ModelMapper modelMapper;

    @Override
    public QuestionnaireDto createQuestionnaire(CreateQuestionnaireVo vo) {
        Questionnaire questionnaire = Questionnaire.builder()
                .title(StringUtils.isBlank(vo.getTitle()) ? Constant.QUESTIONNAIRE.DEFAULT_TITLE :
                        vo.getTitle()).createBy(CurrentUserContext.getUserInfoId())
                .updateBy(CurrentUserContext.getUserInfoId())
                .questionnaireType(vo.getQuestionnaireType()).publishedState(vo.getPublishedState())
                .build();
        questionnaireDao.addQuestionnaire(questionnaire);

        Optional.ofNullable(questionnaire.getId())
                .orElseThrow(BusinessExceptionCode.CREATE_QUESTIONNAIRE_FAIL::exception);

        QuestionnaireDto questionnaireDto = modelMapper.convert(questionnaire);

        questionnaireDto.setCreateBy(CurrentUserContext.getNickName());
        questionnaireDto.setUpdateBy(CurrentUserContext.getNickName());

        return questionnaireDto;
    }

    @Override
    public QuestionnaireDto getQuestionnaire(Long questionnaireId) {
        Questionnaire questionnaire = questionnaireDao.findQuestionnaire(questionnaireId);

        if (Objects.isNull(questionnaire)) {
            throw BusinessExceptionCode.QUESTIONNAIRE_NOT_FOUND.exception();
        }

        QuestionnaireDto dto = modelMapper.convert(questionnaire);
        dto.setCreateBy(Objects.isNull(questionnaire.getCreateBy()) ? null :
                userInfoDao.findNickNameById(questionnaire.getCreateBy()));
        dto.setUpdateBy(Objects.isNull(questionnaire.getUpdateBy()) ? null :
                userInfoDao.findNickNameById(questionnaire.getUpdateBy()));

        dto.setQuestions(questionDao.findQuestionByQuestionnaireId(questionnaireId));

        return dto;
    }

    @Override
    public void updateQuestionnaire(UpdateQuestionnaireVo vo) {
        int updateCount = questionnaireDao.updateQuestionnaire(Questionnaire.builder()
                .id(vo.getId())
                .updateBy(CurrentUserContext.getUserInfoId())
                .publishedState(vo.getPublishedState())
                .questionnaireType(vo.getQuestionnaireType())
                .build());
        if (updateCount <= 0) {
            throw BusinessExceptionCode.QUESTIONNAIRE_UPDATE_FAIL.exception();
        }
    }

    @Override
    public void deleteQuestionnaire(Long questionnaireId) {
        questionnaireDao.updateQuestionnaire(Questionnaire.builder()
                .id(questionnaireId)
                .delFlag(Constant.DELETE_STATUS.IS_DELETE)
                .build());
    }

    @Override
    public void addQuestion(QuestionVo vo) {
        int resultCount = questionDao.addQuestion(Question.builder()
                .questionnaireId(vo.getQuestionnaireId())
                .question(vo.getQuestion())
                .option(vo.getOption())
                .createBy(CurrentUserContext.getUserInfoId())
                .build());
        if (resultCount <= 0) {
            throw BusinessExceptionCode.SAVE_QUESTION_FAIL.exception();
        }
    }

    @Override
    public void updateQuestion(UpdateQuestionVo vo) {
        int updateCount = questionDao.updateQuestion(Question.builder()
                .id(vo.getId())
                .question(vo.getQuestion())
                .option(vo.getOption())
                .build());
        if (updateCount <= 0) {
            throw BusinessExceptionCode.SAVE_QUESTION_FAIL.exception();
        }
    }

    @Override
    public void deleteQuestion(Long questionId) {
        questionDao.updateQuestion(Question.builder()
                .id(questionId)
                .delFlag(Constant.DELETE_STATUS.IS_DELETE)
                .build());
    }


}
