package com.bluea.server.service.impl;

import java.util.List;

import com.bluea.server.common.CurrentUserContext;
import com.bluea.server.dao.AnswerDao;
import com.bluea.server.pojo.entity.Answer;
import com.bluea.server.service.AnswerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AnswerServiceImpl implements AnswerService {

    private final AnswerDao answerDao;

    @Override
    public void insertAnswers(List<Answer> answers) {
        for (Answer answer: answers) {
            answer.setAnswerBy(CurrentUserContext.getUserInfoId());
        }
        answerDao.addAnswers(answers);
    }

    @Override
    public List<Answer> getAnswerByQuestionId(Long questionId) {
        return answerDao.getAnswerByQuestionId(questionId);
    }
}
