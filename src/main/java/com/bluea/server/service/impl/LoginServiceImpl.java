package com.bluea.server.service.impl;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import com.bluea.server.common.Constant;
import com.bluea.server.common.cache.RedisCacheUtil;
import com.bluea.server.common.properties.SurveyServerProperties;
import com.bluea.server.dao.UserInfoDao;
import com.bluea.server.enums.UserInfoStateEnum;
import com.bluea.server.exception.ApplicationException;
import com.bluea.server.exception.BusinessExceptionCode;
import com.bluea.server.pojo.bo.UserInfoBo;
import com.bluea.server.pojo.dto.AccessTokenDto;
import com.bluea.server.pojo.entity.UserInfo;
import com.bluea.server.pojo.vo.LoginUserInfoVo;
import com.bluea.server.pojo.vo.UserInfoVo;
import com.bluea.server.service.LoginService;
import com.bluea.server.util.LocalDateTimeUtil;
import com.bluea.server.util.TokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserInfoDao userInfoDao;
    private final SurveyServerProperties surveyServerProperties;
    private final AuthenticationManager authenticationManager;
    private final RedisCacheUtil redisCacheUtil;
    private final TokenUtil tokenUtil;

    @Override
    public boolean register(UserInfoVo vo) {

        if (userInfoDao.findCountByUsername(vo.getUsername()) > 0) {
            throw BusinessExceptionCode.REGISTER_FAIL.exception("用户名已注册");
        }

        UserInfo userInfo = UserInfo.builder().username(vo.getUsername())
                .password(bCryptPasswordEncoder.encode(vo.getPassword())).nickName(vo.getNickName())
                .email(vo.getEmail()).phone(vo.getPhone()).state(UserInfoStateEnum.NORMAL).build();

        int addUserInfo = userInfoDao.addUserInfo(userInfo);

        if (addUserInfo <= 0) {
            throw BusinessExceptionCode.REGISTER_FAIL.exception("register fail");
        }

        return true;
    }

    @Override
    public AccessTokenDto login(LoginUserInfoVo vo) {
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(vo.getUsername(), vo.getPassword()));
        } catch (Exception e) {
            log.error("用户名或密码错误");
        }

        if (Objects.isNull(authentication)) {
            processFailedLogin(vo);
        }

        return processSuccessLogin(authentication, vo);
    }

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) {
        UserInfo userInfo = userInfoDao.findUserInfoByUsername(username);
        if (Objects.isNull(userInfo)) {
            throw BusinessExceptionCode.INCORRECT_ACCOUNT.exception();
        }
        return new UserInfoBo(userInfo);
    }

    private void processFailedLogin(LoginUserInfoVo vo) throws ApplicationException {
        UserInfo userInfo = userInfoDao.findUserInfoByUsername(vo.getUsername());
        if (userInfo == null || UserInfoStateEnum.DISABLE.equals(userInfo.getState())) {
            throw BusinessExceptionCode.INCORRECT_ACCOUNT.exception();
        }

        if (UserInfoStateEnum.LOCK.equals(userInfo.getState())) {
            throw BusinessExceptionCode.ACCOUNT_LOCK.exception();
        }

        UserInfo newUserInfo = UserInfo.builder().id(userInfo.getId()).build();
        int count = Objects.isNull(userInfo.getLoginFailCount()) ? 0 : userInfo.getLoginFailCount();
        count++;
        if (count >= surveyServerProperties.getLoginMaxFailCount()) {
            newUserInfo.setState(UserInfoStateEnum.LOCK);
        }
        newUserInfo.setLoginFailCount(count);
        newUserInfo.setLoginFailTime(
                LocalDateTimeUtil.nowUtcToLocal(surveyServerProperties.getDefaultTimeZone()));
        userInfoDao.updateUserInfo(newUserInfo);

        int loginTimes = surveyServerProperties.getLoginMaxFailCount() - count;
        throw BusinessExceptionCode.LOGIN_FAIL.exception(
                loginTimes > 0 ? "还有" + loginTimes + "次机会" : "账号已锁定");
    }

    private AccessTokenDto processSuccessLogin(Authentication authentication, LoginUserInfoVo vo) {
        final UserInfoBo principal = (UserInfoBo) authentication.getPrincipal();
        final UserInfo userInfo = principal.getUserInfo();
        redisCacheUtil.setCacheObject(Constant.CACHE.AUTHENTICATION + userInfo.getId(), userInfo,
                tokenUtil.getTokenTtlMs().intValue(), TimeUnit.MILLISECONDS);

        UserInfo updateUserInfo = UserInfo.builder()
                .id(userInfo.getId())
                .lastLoginTime(LocalDateTimeUtil.nowUtcToLocal(
                        surveyServerProperties.getDefaultTimeZone()))
                .loginFailCount(0)
                .build();
        userInfoDao.updateUserInfo(updateUserInfo);

        return AccessTokenDto.builder().token(tokenUtil.createToken(userInfo.getId().toString()))
                .build();
    }
}
