package com.bluea.server.service;

import com.bluea.server.pojo.dto.QuestionnaireDto;
import com.bluea.server.pojo.entity.Question;
import com.bluea.server.pojo.entity.Questionnaire;
import com.bluea.server.pojo.vo.CreateQuestionnaireVo;
import com.bluea.server.pojo.vo.QuestionVo;
import com.bluea.server.pojo.vo.UpdateQuestionVo;
import com.bluea.server.pojo.vo.UpdateQuestionnaireVo;

public interface QuestionnaireService {

    QuestionnaireDto createQuestionnaire(CreateQuestionnaireVo vo);

    QuestionnaireDto getQuestionnaire(Long questionnaireId);

    void updateQuestionnaire(UpdateQuestionnaireVo vo);

    void deleteQuestionnaire(Long questionnaireId);

    void addQuestion(QuestionVo vo);

    void updateQuestion(UpdateQuestionVo vo);

    void deleteQuestion(Long questionId);
}
