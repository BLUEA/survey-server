package com.bluea.server.service;

import com.bluea.server.exception.ApplicationException;
import com.bluea.server.pojo.dto.AccessTokenDto;
import com.bluea.server.pojo.vo.LoginUserInfoVo;
import com.bluea.server.pojo.vo.UserInfoVo;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface LoginService extends UserDetailsService {

    boolean register(UserInfoVo vo);

    AccessTokenDto login(LoginUserInfoVo vo);

}
