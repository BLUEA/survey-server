package com.bluea.server.service;

import java.util.List;

import com.bluea.server.pojo.entity.Answer;

public interface AnswerService {

    void insertAnswers(List<Answer> answers);

    List<Answer> getAnswerByQuestionId(Long questionId);
}
