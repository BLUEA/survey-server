package com.bluea.server.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "survey")
public class SurveyServerProperties {

    private String defaultTimeZone;
    private Integer loginMaxFailCount;

}
