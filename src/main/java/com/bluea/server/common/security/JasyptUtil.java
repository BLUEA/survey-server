package com.bluea.server.common.security;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.encryption.pbe.config.SimpleStringPBEConfig;

public class JasyptUtil {

    private static final String PROVIDER_NAME = "SunJCE";
    private static final String PASSWORD = "com#bluea%survey$0128";
    private static final String PBE_WITH_MD5_AND_DES = "PBEWithMD5AndDES";
    private static final String PBE_WITH_HMAC_SHA512_AND_AES_256 = "PBEWITHHMACSHA512ANDAES_256";
    private static final String IV_GENERATOR_CLASSNAME = "org.jasypt.iv.RandomIvGenerator";
    private static final String SALT_GENERATOR_CLASSNAME = "org.jasypt.salt.RandomSaltGenerator";

    /**
     * jasypt encrypt with PBEWithMD5AndDES
     *
     * @param data
     * @param password
     * @return
     */
    public static String encryptWithMD5(final String data, final String password) {
        final StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        final EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();
        config.setProviderName(PROVIDER_NAME);
        config.setAlgorithm(PBE_WITH_MD5_AND_DES);
        config.setSaltGeneratorClassName(SALT_GENERATOR_CLASSNAME);
        config.setIvGeneratorClassName(IV_GENERATOR_CLASSNAME);
        config.setKeyObtentionIterations("10000");
        config.setStringOutputType("base64");
        config.setPassword(password);
        config.setPoolSize("1");
        encryptor.setConfig(config);

        return encryptor.encrypt(data);
    }


    /**
     * jasypt decrypt with PBEWithMD5AndDES
     *
     * @param encryptedData
     * @param password
     * @return
     */
    public static String decryptWithMD5(final String encryptedData, final String password) {
        final StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        final EnvironmentStringPBEConfig config = new EnvironmentStringPBEConfig();
        config.setProviderName(PROVIDER_NAME);
        config.setAlgorithm(PBE_WITH_MD5_AND_DES);
        config.setSaltGeneratorClassName(SALT_GENERATOR_CLASSNAME);
        config.setIvGeneratorClassName(IV_GENERATOR_CLASSNAME);
        config.setKeyObtentionIterations("10000");
        config.setStringOutputType("base64");
        config.setPoolSize("1");
        config.setPassword(password);
        encryptor.setConfig(config);

        return encryptor.decrypt(encryptedData);
    }


    /**
     * jasypt encrypt with PBEWITHHMACSHA512ANDAES_256
     *
     * @param data
     * @param password
     * @return
     */
    public static String encryptWithSHA512(final String data, final String password) {
        final PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        final SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setProviderName(PROVIDER_NAME);
        config.setAlgorithm(PBE_WITH_HMAC_SHA512_AND_AES_256);
        config.setSaltGeneratorClassName(SALT_GENERATOR_CLASSNAME);
        config.setIvGeneratorClassName(IV_GENERATOR_CLASSNAME);
        config.setKeyObtentionIterations("10000");
        config.setStringOutputType("base64");
        config.setPoolSize("1");
        config.setPassword(password);
        encryptor.setConfig(config);

        return encryptor.encrypt(data);
    }

    /**
     * jasypt decrypt with PBEWITHHMACSHA512ANDAES_256
     *
     * @param encryptedData
     * @param password
     * @return
     */
    public static String decryptWithSHA512(final String encryptedData, final String password) {
        final PooledPBEStringEncryptor encryptor = new PooledPBEStringEncryptor();
        final SimpleStringPBEConfig config = new SimpleStringPBEConfig();
        config.setProviderName(PROVIDER_NAME);
        config.setAlgorithm(PBE_WITH_HMAC_SHA512_AND_AES_256);
        config.setSaltGeneratorClassName(SALT_GENERATOR_CLASSNAME);
        config.setIvGeneratorClassName(IV_GENERATOR_CLASSNAME);
        config.setKeyObtentionIterations("10000");
        config.setStringOutputType("base64");
        config.setPoolSize("1");
        config.setPassword(password);
        encryptor.setConfig(config);

        return encryptor.decrypt(encryptedData);
    }

    public static void main(String[] args) {
        System.out.println(encryptWithSHA512("root", PASSWORD));
        System.out.println(encryptWithSHA512("aaXBRXZ6hyldapbHsJE7rVRQUHk33HZqUd5s92pUuZY4hw5PxSSKGrmeFMi7tpAivf6ctv1A7NAbpz2tQoGLmLiR2ggIK9xAsYYwi5zYdkCU3tD7FpG7uqZH3pXMVUFwEpIQCVTdFjes7Nrni26lJWwQ4vaiC8CqFwahQQ3RgFNuQud406evNeBoVeqQnb23C2cDecPDjzBYdYT8fEFZYhAL1YTm8F3yN5fGR5dZXPkrjoMD2hwHXqCwxErC51Fg", PASSWORD));
    }
}
