package com.bluea.server.common;

import java.util.Optional;

import com.bluea.server.exception.BusinessExceptionCode;
import com.bluea.server.pojo.bo.UserInfoBo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public final class CurrentUserContext {

    public static UserInfoBo getUserInfoBo() {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            return Optional.ofNullable((UserInfoBo) authentication.getPrincipal())
                    .orElseThrow(BusinessExceptionCode.UNAUTHENTICATED::exception);
        } catch (Exception exception) {
            throw BusinessExceptionCode.UNAUTHENTICATED.exception();
        }
    }

    public static Long getUserInfoId() {
        return getUserInfoBo().getUserInfo().getId();
    }

    public static String getUsername() {
        return getUserInfoBo().getUsername();
    }

    public static String getNickName() {
        return getUserInfoBo().getUserInfo().getNickName();
    }

}
