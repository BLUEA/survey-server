package com.bluea.server.common;

public interface Constant {

    interface AUTH {
        String ACCESS_TOKEN = "token";
    }

    interface CACHE {
        String AUTHENTICATION = "Authentication:";
    }

    interface QUESTIONNAIRE {
        String DEFAULT_TITLE = "未命名问卷";
    }

    interface DELETE_STATUS {
        Integer IS_DELETE = 1;
        Integer UN_DELETE = 0;
    }
}
