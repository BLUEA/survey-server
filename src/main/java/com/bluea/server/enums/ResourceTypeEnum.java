package com.bluea.server.enums;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ResourceTypeEnum {

    FILE("FILE", "file")

    ;

    private final String code;
    private final String remark;
}
