package com.bluea.server.enums;

import java.util.Arrays;

import com.bluea.server.exception.ApplicationException;
import com.bluea.server.exception.FrameworkExceptionCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PublishedStateEnum {

    UNPUBLISHED(0, "UNPUBLISHED", "未发布"), PUBLISHED(1, "PUBLISHED", "已发布");

    private final Integer code;
    private final String name;
    private final String remark;

    public static PublishedStateEnum parse(int code) {
        return Arrays.stream(PublishedStateEnum.values())
                .filter(tmp -> tmp.getCode().compareTo(code) == 0).findFirst()
                .orElseThrow(() -> FrameworkExceptionCode.ENUM_PARSE_ERROR.exception(code));
    }

    public static PublishedStateEnum parse(String name) throws ApplicationException {
        return Arrays.stream(PublishedStateEnum.values()).filter(tmp -> tmp.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> FrameworkExceptionCode.ENUM_PARSE_ERROR.exception(name));
    }
}
