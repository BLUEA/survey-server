package com.bluea.server.enums;

import java.util.Arrays;

import com.bluea.server.exception.ApplicationException;
import com.bluea.server.exception.FrameworkExceptionCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserInfoStateEnum {

    LOCK(0, "LOCK", "锁定"),
    NORMAL(1, "NORMAL", "正常"),
    DISABLE(2, "DISABLE", "不可用");

    private final Integer code;
    private final String name;
    private final String remark;

    public static UserInfoStateEnum parse(int code) throws ApplicationException {
        return Arrays.stream(UserInfoStateEnum.values())
                .filter(tmp -> tmp.getCode().compareTo(code) == 0)
                .findFirst()
                .orElseThrow(() -> FrameworkExceptionCode.ENUM_PARSE_ERROR.exception(code));
    }

    public static UserInfoStateEnum parse(String name) throws ApplicationException {
        return Arrays.stream(UserInfoStateEnum.values())
                .filter(tmp -> tmp.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> FrameworkExceptionCode.ENUM_PARSE_ERROR.exception(name));
    }
}
