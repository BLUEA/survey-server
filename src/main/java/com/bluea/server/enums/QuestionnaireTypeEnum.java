package com.bluea.server.enums;

import java.util.Arrays;

import com.bluea.server.exception.ApplicationException;
import com.bluea.server.exception.FrameworkExceptionCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum QuestionnaireTypeEnum {

    ANONYMOUS(0, "ANONYMOUS", "匿名"),
    REAL_NAME(1, "REAL_NAME", "实名");

    private final Integer code;
    private final String name;
    private final String remark;

    public static QuestionnaireTypeEnum parse(int code) throws ApplicationException {
        return Arrays.stream(QuestionnaireTypeEnum.values())
                .filter(tmp -> tmp.getCode().compareTo(code) == 0).findFirst()
                .orElseThrow(() -> FrameworkExceptionCode.ENUM_PARSE_ERROR.exception(code));
    }

    public static QuestionnaireTypeEnum parse(String name) throws ApplicationException {
        return Arrays.stream(QuestionnaireTypeEnum.values())
                .filter(tmp -> tmp.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> FrameworkExceptionCode.ENUM_PARSE_ERROR.exception(name));
    }

}
