package com.bluea.server.pojo.entity;

import java.time.LocalDateTime;

import com.bluea.server.enums.PublishedStateEnum;
import com.bluea.server.enums.QuestionnaireTypeEnum;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Questionnaire {

    private Long id;
    private String title;
    private Long createBy;
    private Long updateBy;
    private QuestionnaireTypeEnum questionnaireType;
    private PublishedStateEnum publishedState;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private Integer delFlag;

}
