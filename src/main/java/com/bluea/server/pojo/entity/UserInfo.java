package com.bluea.server.pojo.entity;

import java.time.LocalDateTime;

import com.bluea.server.enums.UserInfoStateEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

    private Long id;
    private String username;
    private String password;
    private String email;
    private String phone;
    private String nickName;
    private Integer loginFailCount;
    private LocalDateTime loginFailTime;
    private UserInfoStateEnum state;
    private String lastLoginIp;
    private LocalDateTime lastLoginTime;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private Integer delFlag;

}
