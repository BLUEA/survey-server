package com.bluea.server.pojo.entity;

import lombok.Data;

@Data
public class UserResource {

    private Long userid;
    private Long questionnaireId;
    private Long resourceId;

}
