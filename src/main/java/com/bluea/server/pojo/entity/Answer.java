package com.bluea.server.pojo.entity;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
public class Answer {

    private Long id;
    private Long questionId;
    private String answer;
    private Long answerBy;
    private LocalDateTime answerTime;

}
