package com.bluea.server.pojo.entity;

import com.bluea.server.enums.ResourceTypeEnum;
import lombok.Data;

@Data
public class resource {

    private Integer id;
    private String resourceName;
    private String resourceCode;
    private ResourceTypeEnum resourceType;

}
