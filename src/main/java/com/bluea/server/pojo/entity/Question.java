package com.bluea.server.pojo.entity;

import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Question {

    private Long id;
    private Long questionnaireId;
    private String question;
    private JsonNode option;
    private Long createBy;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private Integer delFlag;

}
