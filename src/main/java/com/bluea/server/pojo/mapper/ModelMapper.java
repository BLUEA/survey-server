package com.bluea.server.pojo.mapper;

import com.bluea.server.pojo.dto.QuestionnaireDto;
import com.bluea.server.pojo.entity.Questionnaire;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface ModelMapper {

    ModelMapper INSTANCE = Mappers.getMapper(ModelMapper.class);

    QuestionnaireDto convert(Questionnaire questionnaire);
}
