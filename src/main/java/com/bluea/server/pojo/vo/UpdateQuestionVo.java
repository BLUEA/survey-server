package com.bluea.server.pojo.vo;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class UpdateQuestionVo {
    @NotNull
    private Long id;
    private String question;
    private JsonNode option;
}
