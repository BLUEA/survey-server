package com.bluea.server.pojo.vo;

import javax.validation.constraints.NotNull;

import com.bluea.server.enums.PublishedStateEnum;
import com.bluea.server.enums.QuestionnaireTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateQuestionnaireVo {

    private String title;

    @NotNull
    private QuestionnaireTypeEnum questionnaireType;

    @NotNull
    private PublishedStateEnum publishedState;

}
