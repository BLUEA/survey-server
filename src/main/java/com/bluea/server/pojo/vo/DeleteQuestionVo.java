package com.bluea.server.pojo.vo;

import lombok.Data;

@Data
public class DeleteQuestionVo {
    private Long questionId;
}
