package com.bluea.server.pojo.vo;

import java.io.Serializable;
import javax.validation.constraints.NotBlank;

import lombok.Builder;
import lombok.Data;

@Data
public class UserInfoVo implements Serializable {

    @NotBlank
    private String username;

    @NotBlank
    private String password;

    private String email;

    private String phone;

    private String nickName;

}
