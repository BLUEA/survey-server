package com.bluea.server.pojo.vo;

import lombok.Data;

@Data
public class DeleteQuestionnaireVo {
    private Long id;
}
