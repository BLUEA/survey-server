package com.bluea.server.pojo.vo;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.Data;

@Data
public class QuestionVo {
    @NotNull
    private Long questionnaireId;
    @NotNull
    private String question;

    private JsonNode option;
}
