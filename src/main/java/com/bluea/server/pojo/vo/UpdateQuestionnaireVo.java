package com.bluea.server.pojo.vo;

import javax.validation.constraints.NotNull;

import com.bluea.server.enums.PublishedStateEnum;
import com.bluea.server.enums.QuestionnaireTypeEnum;
import lombok.Data;

@Data
public class UpdateQuestionnaireVo {
    @NotNull
    private Long id;
    private String title;
    private QuestionnaireTypeEnum questionnaireType;
    private PublishedStateEnum publishedState;
}
