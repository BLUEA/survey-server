package com.bluea.server.pojo.vo;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class FindQuestionnaireVo {

    @NotNull
    Long questionnaireId;
}
