package com.bluea.server.pojo.dto;

import java.time.LocalDateTime;
import java.util.List;

import com.bluea.server.enums.PublishedStateEnum;
import com.bluea.server.enums.QuestionnaireTypeEnum;
import com.bluea.server.pojo.entity.Question;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class QuestionnaireDto {
    private Long id;
    private String title;
    private String createBy;
    private String updateBy;
    private List<Question> questions;
    private QuestionnaireTypeEnum questionnaireType;
    private PublishedStateEnum publishedState;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
}
