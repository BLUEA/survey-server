package com.bluea.server.util;

import java.util.Base64;
import java.util.Date;
import java.util.UUID;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TokenUtil {

    @Value("${survey.token.ttl-ms:3600000}")
    private Long tokenTtlMs;

    @Value("${survey.token.issuer:poseidon}")
    private String tokenIssuer;

    @Value("${survey.token.secret}")
    private String tokenSecret;

    public String createToken(final String subject) {
        return getJwtBuilder(subject, getUuid(), tokenTtlMs).compact();
    }

    public Claims parseToken(final String token) {
        final SecretKey secretKey = generalKey();
        return Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody();
    }

    private JwtBuilder getJwtBuilder(final String subject, final String uuid, Long ttlMs) {
        final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        final SecretKey secretKey = generalKey();
        final long nowMillis = System.currentTimeMillis();
        if (ttlMs == null) {
            ttlMs = this.tokenTtlMs;
        }

        return Jwts.builder()
                .setId(uuid)
                .setSubject(subject)
                .setIssuer(tokenIssuer)
                .setIssuedAt(new Date(nowMillis))
                .signWith(signatureAlgorithm, secretKey)
                .setExpiration(new Date(nowMillis + ttlMs));
    }

    private SecretKey generalKey() {
        final byte[] encodedKey = Base64.getDecoder().decode(tokenSecret);
        return new SecretKeySpec(encodedKey, 0, encodedKey.length, "AES");
    }

    public String getUuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public Long getTokenTtlMs() {
        return this.tokenTtlMs;
    }
}
