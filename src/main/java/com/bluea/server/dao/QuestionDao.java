package com.bluea.server.dao;

import java.util.List;

import com.bluea.server.pojo.entity.Question;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QuestionDao {

    List<Question> findQuestionByQuestionnaireId(Long questionnaireId);

    int addQuestion(Question question);

    int updateQuestion(Question question);
}
