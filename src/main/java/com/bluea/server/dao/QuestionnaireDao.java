package com.bluea.server.dao;

import com.bluea.server.pojo.entity.Questionnaire;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface QuestionnaireDao {

    void addQuestionnaire(Questionnaire questionnaire);

    int updateQuestionnaire(Questionnaire questionnaire);

    Questionnaire findQuestionnaire(long id);

}
