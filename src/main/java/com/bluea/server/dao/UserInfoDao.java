package com.bluea.server.dao;

import java.util.List;

import com.bluea.server.pojo.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserInfoDao {

    UserInfo findUserInfoByUsername(@Param("username") String username);

    @Select("SELECT COUNT(*) FROM user_info WHERE username = #{username} AND del_flag = 0")
    Integer findCountByUsername(String username);

    int addUserInfo(UserInfo userInfo);

    int updateUserInfo(UserInfo userInfo);

    @Select("SELECT nick_name FROM user_info WHERE id = #{id} AND del_flag = 0")
    String findNickNameById(Long id);
}
