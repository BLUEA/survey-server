package com.bluea.server.dao;

import java.util.List;

import com.bluea.server.pojo.entity.Answer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface AnswerDao {

    int addAnswers(@Param("list") List<Answer> answers);

    int updateAnswer(Answer answer);

    List<Answer> getAnswerByQuestionId(Long questionId);

}
