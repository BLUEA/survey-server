package com.bluea.server.filter;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bluea.server.common.Constant;
import com.bluea.server.common.cache.RedisCacheUtil;
import com.bluea.server.exception.BusinessExceptionCode;
import com.bluea.server.pojo.bo.UserInfoBo;
import com.bluea.server.pojo.entity.UserInfo;
import com.bluea.server.util.TokenUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Order(-1)
@Component
@RequiredArgsConstructor
public class TokenAuthenticationFilter extends OncePerRequestFilter {

    private final TokenUtil tokenUtil;
    private final RedisCacheUtil redisCacheUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        // 获取token，并解析
        final String token = request.getHeader(Constant.AUTH.ACCESS_TOKEN);
        if (StringUtils.isBlank(token)) {
            filterChain.doFilter(request, response);
            return;
        }
        String userId;
        try {
            userId = tokenUtil.parseToken(token).getSubject();
        } catch (Exception ex) {
            throw BusinessExceptionCode.UNAUTHENTICATED.exception();
        }

        UserInfo cacheObject = redisCacheUtil.getCacheObject(Constant.CACHE.AUTHENTICATION + userId);
        if (Objects.isNull(cacheObject)) {
            throw BusinessExceptionCode.UNAUTHENTICATED.exception();
        }

        UserInfoBo principal = new UserInfoBo(cacheObject);

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(principal, token, null);

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        filterChain.doFilter(request, response);
    }
}
