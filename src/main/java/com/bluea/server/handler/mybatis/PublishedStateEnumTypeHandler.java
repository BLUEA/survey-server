package com.bluea.server.handler.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bluea.server.enums.PublishedStateEnum;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

@MappedTypes(PublishedStateEnum.class)
@MappedJdbcTypes(JdbcType.TINYINT)
public class PublishedStateEnumTypeHandler extends BaseTypeHandler<PublishedStateEnum> {
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i,
                                    PublishedStateEnum questionnaireTypeEnum, JdbcType jdbcType)
            throws SQLException {
        preparedStatement.setInt(i, questionnaireTypeEnum.getCode());
    }

    @Override
    public PublishedStateEnum getNullableResult(ResultSet resultSet, String s)
            throws SQLException {
        return PublishedStateEnum.parse(resultSet.getInt(s));
    }

    @Override
    public PublishedStateEnum getNullableResult(ResultSet resultSet, int i) throws SQLException {
        return PublishedStateEnum.parse(resultSet.getInt(i));
    }

    @Override
    public PublishedStateEnum getNullableResult(CallableStatement callableStatement, int i)
            throws SQLException {
        return PublishedStateEnum.parse(callableStatement.getInt(i));
    }
}
