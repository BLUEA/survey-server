package com.bluea.server.handler.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bluea.server.enums.QuestionnaireTypeEnum;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

@MappedTypes(QuestionnaireTypeEnum.class)
@MappedJdbcTypes(JdbcType.TINYINT)
public class QuestionnaireTypeEnumTypeHandler extends BaseTypeHandler<QuestionnaireTypeEnum> {
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i,
                                    QuestionnaireTypeEnum questionnaireTypeEnum, JdbcType jdbcType)
            throws SQLException {
        preparedStatement.setInt(i, questionnaireTypeEnum.getCode());
    }

    @Override
    public QuestionnaireTypeEnum getNullableResult(ResultSet resultSet, String s)
            throws SQLException {
        return QuestionnaireTypeEnum.parse(resultSet.getInt(s));
    }

    @Override
    public QuestionnaireTypeEnum getNullableResult(ResultSet resultSet, int i) throws SQLException {
        return QuestionnaireTypeEnum.parse(resultSet.getInt(i));
    }

    @Override
    public QuestionnaireTypeEnum getNullableResult(CallableStatement callableStatement, int i)
            throws SQLException {
        return QuestionnaireTypeEnum.parse(callableStatement.getInt(i));
    }
}
