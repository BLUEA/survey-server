package com.bluea.server.handler.mybatis;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bluea.server.enums.UserInfoStateEnum;
import com.bluea.server.exception.ApplicationException;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedJdbcTypes;
import org.apache.ibatis.type.MappedTypes;

@Slf4j
@MappedTypes(UserInfoStateEnum.class)
@MappedJdbcTypes(JdbcType.TINYINT)
public class UserInfoStateEnumTypeHandler extends BaseTypeHandler<UserInfoStateEnum> {
    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i,
                                    UserInfoStateEnum userInfoStateEnum, JdbcType jdbcType)
            throws SQLException {
        preparedStatement.setInt(i, userInfoStateEnum.getCode());
    }

    @Override
    public UserInfoStateEnum getNullableResult(ResultSet resultSet, String s) throws SQLException {
        int code = resultSet.getInt(s);
        try {
            return UserInfoStateEnum.parse(code);
        } catch (ApplicationException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    @Override
    public UserInfoStateEnum getNullableResult(ResultSet resultSet, int i) throws SQLException {
        int code = resultSet.getInt(i);
        try {
            return UserInfoStateEnum.parse(code);
        } catch (ApplicationException e) {
            log.error(e.getMessage());
            return null;
        }
    }

    @Override
    public UserInfoStateEnum getNullableResult(CallableStatement callableStatement, int i)
            throws SQLException {
        int code = callableStatement.getInt(i);
        try {
            return UserInfoStateEnum.parse(code);
        } catch (ApplicationException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
